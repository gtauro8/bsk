package tdd.training.bsk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GameTest {

	@Test
	public void testGetFrameByGame() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(3, 6);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertTrue(f1 == g.getFrameAt(0));

	}

	@Test
	public void testCalculateGameScore() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(3, 6);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(81, g.calculateScore());

	}

	@Test
	public void testCalculateGameScoreWithSpare() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(4, 6);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(89, g.calculateScore());

	}

	@Test
	public void testCalculateGameScoreWithStrike() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(10, 0);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(91, g.calculateScore());

	}

	@Test
	public void testCalculateGameScoreWithStrikeAndSpare() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(10, 0);
		Frame f3 = new Frame(7, 3);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(96, g.calculateScore());

	}

	@Test
	public void testCalculateGameScoreWithMultipleSpare() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(4, 6);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(4, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(5, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(103, g.calculateScore());

	}

	@Test
	public void testCalculateGameScoreWithMultipleStrike() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(10, 0);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(10, 0);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(99, g.calculateScore());

	}

	@Test
	public void testCalculateGameScoreWithASpareAtLastFrame() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(8, 1);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(2, 6);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(4, 6);

		g.setFirstBonusThrow(3);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(86, g.calculateScore());

	}

	@Test
	public void testCalculateGameScoreWithAStrikeAtLastFrame() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(8, 1);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(2, 6);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(10, 0);

		g.setFirstBonusThrow(3);
		g.setSecondBonusThrow(3);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(89, g.calculateScore());

	}

	@Test
	public void testBestScore() throws BowlingException {
		Game g = new Game();
		Frame f1 = new Frame(10, 0);
		Frame f2 = new Frame(10, 0);
		Frame f3 = new Frame(10, 0);
		Frame f4 = new Frame(10, 0);
		Frame f5 = new Frame(10, 0);
		Frame f6 = new Frame(10, 0);
		Frame f7 = new Frame(10, 0);
		Frame f8 = new Frame(10, 0);
		Frame f9 = new Frame(10, 0);
		Frame f10 = new Frame(10, 0);

		g.setFirstBonusThrow(3);
		g.setSecondBonusThrow(3);

		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);

		assertEquals(300, g.calculateScore());

	}

}
