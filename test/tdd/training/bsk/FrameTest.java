package tdd.training.bsk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FrameTest {

	@Test
	public void testGetFirstThrow() throws BowlingException {
		int inputFirstThrow = 5;
		int inputSecondThrow = 3;

		Frame f = new Frame(inputFirstThrow, inputSecondThrow);
		assertTrue(5 == f.getFirstThrow());

	}

	@Test
	public void testGetSecondThrow() throws BowlingException {
		int inputFirstThrow = 5;
		int inputSecondThrow = 3;
		Frame f = new Frame(inputFirstThrow, inputSecondThrow);
		assertTrue(3 == f.getSecondThrow());

	}

	@Test
	public void testCalculatingScore() throws BowlingException {
		int inputFirstThrow = 5;
		int inputSecondThrow = 3;
		Frame f = new Frame(inputFirstThrow, inputSecondThrow);
		assertTrue(8 == f.getScore());

	}

	@Test
	public void testIsSpare() throws BowlingException {
		Frame f1 = new Frame(5, 5);
		assertEquals(10, f1.getScore());

	}

	@Test
	public void testIsStrike() throws BowlingException {
		Frame f1 = new Frame(10, 0);
		assertTrue(10 == f1.getFirstThrow());

	}

}
