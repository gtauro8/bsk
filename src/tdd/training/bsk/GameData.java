package tdd.training.bsk;

import java.util.ArrayList;

public class GameData {
	public int firstThrow;
	public int secondThrow;
	public ArrayList<Frame> game;

	public GameData(ArrayList<Frame> game) {
		this.game = game;
	}
}