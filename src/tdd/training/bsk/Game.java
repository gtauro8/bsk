package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	private GameData data = new GameData(null);

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {

		this.data.game = new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {

		data.game.add(frame);

	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {

		return data.game.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {

		this.data.firstThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {

		this.data.secondThrow = secondBonusThrow;

	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {

		return data.firstThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {

		return data.secondThrow;

	}

	public int calculateScoreWhenIsStrike(int index, int sum) throws BowlingException {
		if (index == data.game.size() - 1) {

			setFirstBonusThrow(data.firstThrow);
			setSecondBonusThrow(data.secondThrow);
			getFrameAt(index).setBonus(getFirstBonusThrow() + getSecondBonusThrow());
			sum += getFrameAt(index).getScore();
		} else {
			getFrameAt(index).setBonus(getFrameAt(index + 1).getScore());
			sum += getFrameAt(index).getScore();
		}

		return sum;
	}

	public int calculateScoreWhenIsSpare(int index, int sum) throws BowlingException {

		if (index == (data.game.size() - 1)) {

			setFirstBonusThrow(data.firstThrow);
			getFrameAt(index).setBonus(getFirstBonusThrow());
			sum += getFrameAt(index).getScore();

		} else {
			getFrameAt(index).setBonus(getFrameAt(index + 1).getFirstThrow());
			sum += getFrameAt(index).getScore();
		}

		return sum;

	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int sum = 0;
		int bestScore = 0;
		for (int i = 0; i < data.game.size(); i++) {

			if (getFrameAt(i).getScore() == 10) {

				if (getFrameAt(i).isStrike()) {
					bestScore++;
					sum = calculateScoreWhenIsStrike(i, sum);

				} else if (getFrameAt(i).isSpare()) {

					sum = calculateScoreWhenIsSpare(i, sum);
				}

			} else {
				sum += getFrameAt(i).getScore();
			}

			if (bestScore == data.game.size()) {
				sum = 300;
			}

		}

		return sum;

	}

}
